/** This is going to check and make sure the donation can go through and will check to see if you donated more than the answer needs.
 * @param {integer} donation - how much the user is trying to donate to the question.
 * @param {integer} centsFunded - how much cents have been funded so far to the question.
 * @param {integer} centsPrice - how much it costs for the answer to be succesffuly funded.
 * @return {boolean} returns true if the user is an influencer, false if they are not.
 * trying to change
 */

module.exports = function(donation, centsFunded, centsPrice) {

    // keeps track of how much funds we need to complete the funding.
    let neededFunds = (centsPrice - centsFunded);
    if(neededFunds <= 0) {
        return({
            success: false,
            error: 'The question does not need anymore funding.'
        })
    }

    // if the donation is more than how much the question needs to be funded, itll cut off the excess donation.
    // and just return the donation as what we needed.
    if (donation > neededFunds) {
        return({
            success: true,
            donation: neededFunds,
        })
    }

    // if it gets here, everything is okay and we can continue with the given donated ammount.
    return({
        success: true,
        donation,
    })
}