// During the test the env variable is set to test
process.env.NODE_ENV = 'test';

const bcrypt = require('bcryptjs');

// Require the dev-dependencies
const chai = require('chai');
const expect = require('chai').expect;
const chaiHttp = require('chai-http');
const request = require('supertest');

const User = require('../src/models/User');
const Answer = require('../src/models/Answer');
const Question = require('../src/models/Question');
const server = require('../src/server');
const db = require('../src/db');

chai.use(chaiHttp);
// Our parent block

describe('Answer', () => {
    // first make an account
    // make a second account
    // login to the first account (questioned) we made
    // create a group
    // log into the second (asker) account
    // ask a question
    let token;
    let test_question;
    beforeEach((done) => { // Before each test we empty the database
        db.connection.sync({force: true}) // drops all the tables and re-creates them
            .then(() => {
                const password = bcrypt.hashSync('test2', 6);

                // first make an account

                return User.create({
                    username: 'questioned',
                    password,
                    email: 'testy@test.com',
                }).then(() => {
                        // make a second account
                        User.create({
                            username: 'asker',
                            password,
                            email: 'testy@tes2t.com',
                            centsOnAccount: 10000,
                        }).then(() => {
                            request(server)
                            //login to the asker
                                .post('/api/login')
                                .send({
                                    username: 'asker',
                                    password: 'test2',
                                })
                                .end((err2, res2) => {
                                    token = res2.body.token;
                                    expect(res2).to.have.status(200);
                                    expect(res2).to.be.a('object');
                                    expect(res2.body).to.have.property('token');
                                    Question.create({
                                        content: 'Yo whats up?',
                                        AskerId: 2,
                                        AnswererId: 1,
                                    }).then((question)=> {
                                        test_question = question;
                                        expect(question.content).to.equal('Yo whats up?');
                                        done();
                                    })
                                })
                        })
                    })

            })
            .catch(error => {
                done(error);
            });
    });

    describe('/PUT/answer/donate', () => {
        it('it should add centsFunded by donation to an answer for a question in basket, and also subtract centsOnAccount from the user making the donation', (done) => {
            //first we need to manually create an answer in the database.
            Answer.create({
                answerURL: 'dontmatter',
                UserId: 2,
                centsPrice: 10000,
            }).then((answer) => {
                //update the Question's answer id
                expect(answer.dataValues.centsPrice).to.equal(10000);
                test_question.update({
                    AnswerId: answer.dataValues.id,
                    status: 'inBasket'
                }).then(() => {
                    request(server)
                    // login to the first account we made and create the group
                        .put('/api/answer/donate')
                        .set('Authorization', token)
                        .send({
                            donation: 2000,
                            questionId: 1,
                        })
                        .end((err, res) => {
                            console.log('the res.body is', res.body);
                            token = res.body.token;
                            expect(res).to.have.status(200);
                            expect(res).to.be.a('object');
                            expect(res.body).to.have.property('data');
                            expect(res.body.data).to.have.property('centsOnAccount');
                            expect(res.body.data).to.have.property('centsFunded');
                            expect(res.body.data).to.have.property('status');
                            expect(res.body.data.centsOnAccount).to.equal(8000);
                            expect(res.body.data.centsFunded).to.equal(2000);
                            expect(res.body.data.status).to.equal("inBasket");
                            done();
                        })
                })
            })
        })
    })


    describe('/PUT/answer/donate', () => {
        it.only('makes a 5k donation, checks to make sure everything gets added/subtracted correctly, then makes another 5k donation to ' +
            'make sure it adds to their donation and only takes 4k from you since thats all it needs for funding ' +
            'and the status of the question is changed', (done) => {
            //first we need to manually create an answer in the database.
            Answer.create({
                answerURL: 'dontmatter',
                UserId: 2,
                centsPrice: 9000,
            }).then((answer) => {
                //update the Question's answer id
                expect(answer.dataValues.centsPrice).to.equal(9000);
                test_question.update({
                    AnswerId: answer.dataValues.id,
                    status: 'inBasket'
                }).then(() => {
                    request(server)
                    // login to the first account we made and create the group
                        .put('/api/answer/donate')
                        .set('Authorization', token)
                        .send({
                            donation: 5000,
                            questionId: 1,
                        })
                        .end((err, res) => {
                            console.log('the res.body is', res.body);
                            expect(res).to.have.status(200);
                            expect(res).to.be.a('object');
                            expect(res.body).to.have.property('data');
                            expect(res.body.data).to.have.property('centsOnAccount');
                            expect(res.body.data).to.have.property('centsFunded');
                            expect(res.body.data).to.have.property('status');
                            expect(res.body.data).to.have.property('centsYouFunded');
                            expect(res.body.data.centsOnAccount).to.equal(5000);
                            expect(res.body.data.centsFunded).to.equal(5000);
                            expect(res.body.data.centsYouFunded).to.equal(5000);
                            expect(res.body.data.status).to.equal("inBasket");
                            request(server)
                            // login to the first account we made and create the group
                                .put('/api/answer/donate')
                                .set('Authorization', token)
                                .send({
                                    donation: 5000,
                                    questionId: 1,
                                })
                                .end((err2, res2) => {
                                    console.log('the res.body is', res.body);
                                    token = res2.body.token;
                                    expect(res2).to.have.status(200);
                                    expect(res2).to.be.a('object');
                                    expect(res2.body).to.have.property('data');
                                    expect(res2.body.data).to.have.property('centsOnAccount');
                                    expect(res2.body.data).to.have.property('centsFunded');
                                    expect(res2.body.data).to.have.property('status');
                                    expect(res2.body.data.centsOnAccount).to.equal(1000);
                                    expect(res2.body.data.centsFunded).to.equal(9000);
                                    expect(res2.body.data.centsYouFunded).to.equal(9000);
                                    expect(res2.body.data.status).to.equal("onTable");
                                    done();
                                })
                        })
                })
            })
        })
    })

    describe('/PUT/answer/donate', () => {
        it('it should return an error if you try to donate more money than you have', (done) => {
            //first we need to manually create an answer in the database.
            Answer.create({
                answerURL: 'dontmatter',
                UserId: 2,
                centsPrice: 15000,
            }).then((answer) => {
                //update the Question's answer id
                test_question.update({
                    AnswerId: answer.dataValues.id,
                    status: 'inBasket'
                }).then(() => {
                    request(server)
                    // login to the first account we made and create the group
                        .put('/api/answer/donate')
                        .set('Authorization', token)
                        .send({
                            donation: 12000,
                            questionId: 1,
                        })
                        .end((err, res) => {
                            console.log('the res.body is', res.body);
                            token = res.body.token;
                            expect(res).to.have.status(500);
                            expect(res).to.be.a('object');
                            expect(res.body).to.have.property('error');
                            expect(res.body.error).to.equal('Insufficent funds on your account.')
                            done();
                        })
                })
            })
        })
    })


    describe('/PUT/answer/donate', () => {
        it('it should return an error if the question isnt answered and the status is "cooking"', (done) => {
            //first we need to manually create an answer in the database.
            Answer.create({
                answerURL: 'dontmatter',
                UserId: 2,
                centsPrice: 15000,
            }).then((answer) => {
                //update the Question's answer id
                test_question.update({
                    AnswerId: answer.dataValues.id,
                    status: 'cooking'
                }).then(() => {
                    request(server)
                    // login to the first account we made and create the group
                        .put('/api/answer/donate')
                        .set('Authorization', token)
                        .send({
                            donation: 10000,
                            questionId: 1,
                        })
                        .end((err, res) => {
                            console.log('the res.body is', res.body);
                            token = res.body.token;
                            expect(res).to.have.status(500);
                            expect(res).to.be.a('object');
                            expect(res.body).to.have.property('error');
                            expect(res.body.error).to.equal('Question is not answered yet and has no price.')
                            done();
                        })
                })
            })
        })
    })

    describe('/PUT/answer/donate', () => {
        it('it should return an error if the question is already funded and the status is "onTable"', (done) => {
            //first we need to manually create an answer in the database.
            Answer.create({
                answerURL: 'dontmatter',
                UserId: 2,
                centsPrice: 15000,
            }).then((answer) => {
                //update the Question's answer id
                test_question.update({
                    AnswerId: answer.dataValues.id,
                    status: 'onTable'
                }).then(() => {
                    request(server)
                    // login to the first account we made and create the group
                        .put('/api/answer/donate')
                        .set('Authorization', token)
                        .send({
                            donation: 10000,
                            questionId: 1,
                        })
                        .end((err, res) => {
                            console.log('the res.body is', res.body);
                            token = res.body.token;
                            expect(res).to.have.status(500);
                            expect(res).to.be.a('object');
                            expect(res.body).to.have.property('error');
                            expect(res.body.error).to.equal('Question is already succesfully funded.')
                            done();
                        })
                })
            })
        })
    })


    describe('/POST/answer/upload', () => {
        it('it should throw an error when we upload a answer with no file', (done) => {
            // subscribe to the channel
            request(server)
            // login to the first account we made and create the group
                .post('/api/login')
                .send({
                    username: 'questioned',
                    password: 'test2',
                })
                .end((err, res) => {
                    token = res.body.token;
                    expect(res).to.have.status(200);
                    expect(res).to.be.a('object');
                    expect(res.body).to.have.property('token');

                    const answer = {
                        questionId: 1,
                    }
                    request(server)
                        .post('/api/answer/upload')
                        .set('Authorization', token)
                        .send(answer)
                        .end((err2, res2) => {
                            console.log('the error was', res2.body.error);
                            console.log(res2.body);
                            expect(res2).to.have.status(500);
                            done();
                        });
                    })

        });
    });

    describe('/POST/answer/create', () => {
        it('should create the video in the database if the video is under 60 seconds', (done) => {
            // subscribe to the channel
            request(server)
            // login to the first account we made and create the group
                .post('/api/login')
                .send({
                    username: 'questioned',
                    password: 'test2',
                })
                .end((err, res) => {
                    token = res.body.token;
                    expect(res).to.have.status(200);
                    expect(res).to.be.a('object');
                    expect(res.body).to.have.property('token');

                    const answer = {
                        questionId: 1,
                        videoKey: "shortvideo.mp4",
                    }
                    request(server)
                        .post('/api/answer/create')
                        .set('Authorization', token)
                        .send(answer)
                        .end((err2, res2) => {
                            console.log('the error was', res2.body.error);
                            console.log(res2.body);
                            expect(res2).to.have.status(200);
                            done();
                        });
                })

        });
    });
/*
    describe('/POST/answer/', () => {
        it('it should not post an answer if the question doesnt exist', (done) => {
            // subscribe to the channel
            request(server)
            // login to the first account we made and create the group
                .post('/api/login')
                .send({
                    username: 'influencer_test',
                    password: 'test2',
                })
                .end((err, res) => {
                    token = res.body.token;
                    expect(res).to.have.status(200);
                    expect(res).to.be.a('object');
                    expect(res.body).to.have.property('token');

                    const answer = {
                        content: 'green becuz broccoli',
                        questionId: 1435,
                    }
                    request(server)
                        .post('/api/answer/create')
                        .set('Authorization', token)
                        .send(answer)
                        .end((err2, res2) => {
                            expect(res2).to.have.status(500);
                            console.log('the body -', res2.body);
                            expect(res2).to.be.a('object');
                            expect(res2.body).to.have.property('error');
                            expect(res2.body.error).to.equal('Question does not exist.');
                            done();
                        });
                })

        });
    });


    describe('/POST/answer/', () => {
        it('it should not post an answer if were not the one being asked', (done) => {
            // subscribe to the channel
            request(server)
            // login to the first account we made and create the group
                const answer = {
                    content: 'green becuz broccoli',
                    questionId: 1,
                }
                request(server)
                    .post('/api/answer/create')
                    .set('Authorization', token)
                    .send(answer)
                    .end((err2, res2) => {
                        expect(res2).to.have.status(500);
                        expect(res2).to.be.a('object');
                        console.log('the body -', res2.body);
                        expect(res2.body).to.have.property('error');
                        expect(res2.body.error).to.equal('This question was not asked to you.');
                        done();
                    });
            })
    });

    describe('/POST/answer/', () => {
        it('it should not post an answer if the question is already answered', (done) => {
            // subscribe to the channel
            request(server)
            // login to the first account we made and create the group
                .post('/api/login')
                .send({
                    username: 'influencer_test',
                    password: 'test2',
                })
                .end((err, res) => {
                    token = res.body.token;
                    expect(res).to.have.status(200);
                    expect(res).to.be.a('object');
                    expect(res.body).to.have.property('token');

                    const answer = {
                        content: 'green becuz broccoli',
                        questionId: 1,
                    }
                    request(server)
                        .post('/api/answer/create')
                        .set('Authorization', token)
                        .send(answer)
                        .end((err2, res2) => {
                            console.log('the error was', res2.body.error);
                            expect(res2).to.have.status(200);
                            expect(res2).to.be.a('object');
                            expect(res2.body).to.have.property('content');
                            expect(res2.body.content).to.equal('green becuz broccoli');
                            request(server)
                                .post('/api/answer/create')
                                .set('Authorization', token)
                                .send(answer)
                                .end((err3, res3) => {
                                    console.log('the error was', res3.body.error);
                                    console.log(res3.body);
                                    expect(res3).to.have.status(500);
                                    expect(res3).to.be.a('object');
                                    expect(res3.body).to.have.property('error');
                                    console.log('the body -', res3.body);
                                    expect(res3.body.error).to.equal('Question is already answered.');
                                    done();
                                });
                        });

                })

        });
    });
    */
});
