const Sequelize = require('sequelize');

const db = require('../db');
const User = require('./User');
const Answer = require('./Answer');

const Transaction = db.connection.define('Transactions', {
    type: {
        type: Sequelize.ENUM('crowdfund', 'purchase'),
        unique: false,
        allowNull: false,
    },
    ammount: {
        type: Sequelize.INTEGER,
        unique: false,
        allowNull: false,
    },
});
Transaction.belongsTo(Answer, { as: 'Answer' });
Transaction.belongsTo(User, { as: 'User' });

module.exports = Transaction;
