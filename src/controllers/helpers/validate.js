/** This is going to be the function that returns true if were a subscriber OR the creator and false if were not.
 * @param {sequelize instance} user - user who is trying to get the posts
 * @param {sequelize instance} group - the group were trying to get the posts from.
 * @return {boolean} returns true if the user is an influencer, false if they are not.
 * trying to change
 */
module.exports.checkGroupAccess = async function (user, group) {
    console.log('the group', group);
    const foundGroup = await user.getGroups({
        where: {
            id: group.dataValues.id,
        },
    });

    if (foundGroup[0] === null || foundGroup[0] === undefined) {
        // we need to now check to see if were the creator
        if (user.dataValues.id !== group.dataValues.CreatorId) {
            // were not the owner of the group so return false
            return false;
        }
    }
    return true;
};

module.exports.isGroupInfluencer = async function(userId, groupCreatorId){
    if (userId === groupCreatorId) {
        return true;
    }
    return false;
}


module.exports.objExists = function (obj){
    if (obj === null || obj === undefined){
      return false;
    }
    return true;
}