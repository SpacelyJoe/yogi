const aws = require('aws-sdk'),
    config = require('config'),
    crypto = require('crypto');


module.exports = async function(file_type) {

    aws.config.update({accessKeyId: config.AWS_ACCESS_KEY, secretAccessKey: config.AWS_SECRET_KEY})

    const s3 = new aws.S3();

    try {
        console.log('gettin this far');
        if (!file_type === "video/mp4") {
            return ({success: false, error: 'Please provide a valid video format'});
        }

        let buffer = await crypto.randomBytes(12);

        let key = buffer.toString('hex');

        let options = {
            Bucket: config.AWS_S3_BUCKET,
            Key: key,
            Expires: 60,
            ContentType: req.query.file_type,
            ACL: 'public-read',
        }

        let data = await s3.getSignedUrl('putObject', options);

        return ({
            success: true,
            signed_request: data,
            url: ('https://s3.amazonaws.com/' + config.AWS_S3_BUCKET + '/' + key),
            key,
        });
    } catch (error) {
        return ({
            success: false,
            error: error.message,
        })
    }
}