const config = require('config');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const User = require('./models/User');

module.exports = function (passport) {
    const opts = {};
    opts.jwtFromRequest = ExtractJwt.fromAuthHeader();
    opts.secretOrKey = config.secretAuthKey;
    passport.use(new JwtStrategy(opts, (jwtPayload, done) => {
        User.findOne({
            where: { id: jwtPayload.id },
        }).then(user => {
            if (user) {
                done(null, user);
            } else {
                done(null, false);
            }
        }).catch((err) => { return done(err, false) });
    }));
};

/*
passport.use(new LocalStrategy.Strategy((username, password, done) => {
    console.log('the username we get is', username);

    User.findOne({
        where: { username },
    }).then(user => {
        bcrypt.compare(password, user.password, (err, isMatch) => {
            if (err) throw err;
            if (isMatch) {
                return done(null, { id: user.id, username: user.username });
            }
            return done(null, false, { message: 'Invalid password' });
        });
    }).catch(err => {
        console.log(err);
        return done(null, false, { message: 'Unknown User' });
    });
}));

passport.serializeUser((user, done) => {
    done(null, user.id);
});

passport.deserializeUser((id, done) => {
    User.findById(id).then(user => {
        done(null, user);
    });
});
*/
