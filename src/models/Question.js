const Sequelize = require('sequelize');

const db = require('../db');
const User = require('./User');
const Answer = require('./Answer');

const Question = db.connection.define('Questions', {
    content: {
        type: Sequelize.STRING,
        unique: false,
        allowNull: true,
    },
    status: {
        type: Sequelize.ENUM('inBasket', 'onTable', 'cooking'),
        unique: false,
        allowNull: false,
        defaultValue: 'cooking',
    },
    upvotes: {
        type: Sequelize.INTEGER,
        unique: false,
        allowNull: false,
        defaultValue: 0,
    },
    // this will only change when we first create the question, when the question gets answered, and when the question gets successfully funded. This is used when we want to query by new instead of top. 
    significantlyUpdatedAt: {
        type: Sequelize.DATE,
        unique: false,
        allowNull: false,
        defaultValue: Sequelize.NOW,
    }
});
Question.belongsTo(User, { as: 'Asker' });
Question.belongsTo(User, { as: 'Answerer' });
Question.belongsTo(Answer, { as: 'Answer'});

module.exports = Question;
