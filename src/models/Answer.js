const Sequelize = require('sequelize');

const db = require('../db');
const User = require('./User');

const Answer = db.connection.define('Answers', {
    answerURL: {
        type: Sequelize.STRING,
        unique: false,
        allowNull: false,
    },
    centsPrice: {
        type: Sequelize.INTEGER,
        unique: false,
        allowNull: false,
        defaultValue: 0,
    },
    centsFunded: {
        type: Sequelize.INTEGER,
        unique: false,
        allowNull: false,
        defaultValue: 0,
    },
});

Answer.belongsTo(User);

module.exports = Answer;
