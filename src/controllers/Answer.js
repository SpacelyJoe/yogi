const Answer = require('../models/Answer');
const Transaction = require('../models/Transaction');
const User = require('../models/User');
const Question = require('../models/Question');
const UserQuestion = require('../models/UserQuestion');
const validate = require('./helpers/validate');
const uploadVideo = require('./helpers/uploadVideo');
const validateVideo = require('./helpers/validateVideo');
const validateDonation = require('./helpers/validateDonation');

/** this will upload an answer to the s3 bucket if theres no answer yet.
 * @param {sequelize instance} answeringUser - ID of the user trying to answer the question.
 * @param {integer} questionId - ID of the question trying to be answered
 * @param {string} file_type - the content of the answer.
 * @return {JSON} returns success true and the question if it worked, and success false and an error message
 * if it doesnt work.
 */
async function upload(answeringUser, questionId, file_type) {
    try {
        const question = await Question.findById(questionId);

        // makes sure the question exists.

        if(!validate.objExists(question)){
            return {
                success: false,
                error: 'Question does not exist.'
            }
        }

        // makes sure the question isn't already answered
        if (question.dataValues.answered) {
            return {
                success: false,
                error: 'Question is already answered.'
            }
        }

        // makes sure that the person trying to answer the question is the one it got asked to.
        if (question.dataValues.AnswererId !== answeringUser.id) {
            return {
                success: false,
                error: 'This question was not asked to you.'
            }
        }
        
        //upload the answer
        const upload = uploadVideo(file_type)
        if(!upload.success){
            return ({
                success: false,
                error: upload.error,
            });
        }

        /*
        // create the answer
        const answer = await Answer.create({
            content,
            UserId: answeringUser.id,
            QuestionId: question.dataValues.id,
        });

        // update that the question has been answered
        await question.update({
            answered: true,
        });
        */

        return ({
            success: true,
            signed_request: upload.signed_request,
            url: upload.url,
        });
    } catch (error) {
        return ({
            success: false,
            error: error.message,
        });
    }
}


/** this will upload an answer to the s3 bucket if theres no answer yet.
 * @param {sequelize instance} donatingUser - ID of the user trying to donate to the question.
 * @param {integer} questionId - ID of the question trying to be donated too.
 * @param {integer} donation - this is the number were trying to donate to the answer.
 * @return {JSON} returns success true and the question if it worked, and success false and an error message
 * if it doesnt work.
 */
async function donate(donatingUser, questionId, donation){
    try{
        //first check to make sure the donatingUser has the funds.
        if(donatingUser.centsOnAccount < donation) {
            return({
                success: false,
                error: 'Insufficent funds on your account.'
            })
        }

        // get the question, make sure it exists and the status is "inBasket" or else you cant donate
        const question = await Question.findOne({
            include: [{
                model: Answer,
                as: 'Answer',
                attributes: ['centsPrice', 'centsFunded', 'id'],
            }],
            where: {
                id: questionId
            }
        });

        // makes sure the question exists.
        if(question === null || question === undefined) {
            return {
                success: false,
                error: 'Question does not exist.'
            }
        }

        // make sure the status of the question isn't cooking or onTable. if it's not then the only other option is inBasket and we can continue.
        let questionStatus = question.dataValues.status;
        switch(questionStatus) {
            case 'cooking':
                return ({
                    success: false,
                    error: 'Question is not answered yet and has no price.'
                })
            case 'onTable':
                return ({
                    success: false,
                    error: 'Question is already succesfully funded.'
                })
        }

        // check to see if we've already donated or have a relationship to the question.
        const userquestion = await UserQuestion.findOrCreate({
            where: {
                QuestionId: questionId,
                UserId: donatingUser.id,
            }
        });
        // this will either be 0 or how ever much we've donated so far.
        const donatedCents = userquestion[0].dataValues.donatedCents;

        const centsFunded = question.dataValues.Answer.dataValues.centsFunded;
        const centsPrice = question.dataValues.Answer.dataValues.centsPrice;


        const donationCheck = validateDonation(donation, centsFunded, centsPrice);
        if(!donationCheck.success){
            return ({
                success: false,
                error: donationCheck.error
            });
        }
        // assign the donation variable to whatever our donationCheck gives us.
        let validatedDonation = donationCheck.donation;

        let [answer, user, userquestion2] = await Promise.all([
                                                        //update the answer to the new funding ammount.
                                                        question.dataValues.Answer.update({ centsFunded: centsFunded + validatedDonation }),

                                                        // update the user's cents from what he donated.
                                                        donatingUser.update({ centsOnAccount: donatingUser.centsOnAccount - validatedDonation }),

                                                        // update the userquestion relationship with how much cents you donated and set the access to true.
                                                        userquestion[0].update({ donatedCents: donatedCents + validatedDonation, hasAccess: true }),

                                                        // create the transaction record.
                                                        Transaction.create({ AnswerId: question.dataValues.Answer.id,
                                                                            UserId: donatingUser.id,
                                                                            type: 'crowdfund',
                                                                            ammount: validatedDonation})
                                                        ]);

        // after we update the cents paid we want to check if it went over or equals the price. if it is we need to update the question's status to onTable.
        // we don't need to do this asynchronously because it doesnt matter when the question updates, it wont affect the response object.
        // we can just tell the user that the status got changed before it actually does the change since we're sending them back questionStatus
        if (answer.dataValues.centsFunded >= answer.dataValues.centsPrice) {
            questionStatus = 'onTable';
            question.update({
                status: 'onTable'
            })

        }

        return({
            success: true,
            data: {
                centsOnAccount: user.dataValues.centsOnAccount,
                centsFunded: answer.dataValues.centsFunded,
                centsYouFunded: userquestion2.dataValues.donatedCents,
                status: questionStatus,
            }
        });

    } catch (error) {
        return({
            success: false,
            error: error.message,
        })
    }
}


/** this will upload an answer to the s3 bucket if theres no answer yet.
 * @param {sequelize instance} answeringUser - ID of the user trying to answer the question.
 * @param {integer} questionId - ID of the question trying to be answered
 * @param {string} file_type - the type of file the user is trying to upload.
 * @param {integer} priceInCents - this is the number of cents that need to be raised for the question to become "onTable" or unlocked.
 * @param {int}
 * @return {JSON} returns success true and the question if it worked, and success false and an error message
 * if it doesnt work.
 */
async function create(answeringUser, questionId, file_type, priceInCents) {
    try {

        const question = await Question.findById(questionId);

        // makes sure the question exists.
        if(question === null || question === undefined) {
            return {
                success: false,
                error: 'Question does not exist.'
            }
        }

        // makes sure the question's status is cooking, otherwise you can't answer.
        if(question.dataValues.status !== 'cooking'){
            if (question.dataValues.answered) {
                return {
                    success: false,
                    error: 'Question is already answered.'
                }
            }
        }

        // makes sure that the person trying to answer the question is the one it got asked to.
        if (question.dataValues.AnswererId !== answeringUser.id) {
            return {
                success: false,
                error: 'This question was not asked to you.'
            }
        }
        
        // upload the answer
        const uploaded = await uploadVideo(req.query.file_type);

        // validate the answer
        /*
        console.log('the videokey here is', videoKey);
        const validated = await validateVideo(61, videoKey);
        console.log('validate here is ', validated);
        */
        
        if(!uploaded.success) {
            return({
                success: false,
                error: uploaded.error,
            })
        }
        
         // create the answer
         const answer = await Answer.create({
             answerURL: uploaded.url,
             UserId: answeringUser.id,
             centsPrice: priceInCents,
         });

         // update that the question to "inBasket" since its been answered but unrevealed.
         await question.update({
             status: 'inBasket',
             AnswerId: answer.dataValues.id,
         });

        return ({
            success: true,
            answer: answer.dataValues,
        });
    } catch (error) {
        return ({
            success: false,
            error: error.message,
        });
    }
}

// -----------------------------------------------------------------------------------------------------
// these are the functions that our routes will use and work as a middle man
// passing in the variables needed for the actual controller functions
// ------------------------------------------------------------------------------------------------------

module.exports.uploadAPI = async function(req, res) {
    if (!req.body.questionId) {
        return (res.send(500).send({ success: false, error: 'Please provide the valid parameters' }));
    }

    req.sanitize('questionId').trim();
    req.sanitize('questionId').escape();

    try {
        const uploaded = await upload(req.user, req.body.questionId, req.query.file_type);
        if (uploaded.success) {
            return res.json({
                signed_request: uploaded.signed_request,
                url: uploaded.url,
                key: uploaded.key,
            });
        }
        return res.status(500).send({ error: uploaded.error });
    } catch (error) {
        return res.status(500).send({ error: error.message });
    }
};


module.exports.createAPI = async function(req, res) {
    if (!req.body.questionId) {
        return (res.send(500).send({ success: false, error: 'Please provide the valid parameters' }));
    }

    req.sanitize('questionId').trim();
    req.sanitize('questionId').escape();

    try {
        const created = await create(req.user, req.body.questionId, req.query.file_type, req.body.priceInCents)
        if (created.success) {
            return res.json({
                answer: created.answer,
            });
        }
        return res.status(500).send({ error: created.error });
    } catch (error) {
        return res.status(500).send({ error: error.message });
    }
};

module.exports.donateAPI = async function(req, res) {
    if (!req.body.questionId || !req.body.donation) {
        return (res.send(500).send({ success: false, error: 'Please provide the valid parameters' }));
    }

    req.sanitize('questionId').trim();
    req.sanitize('questionId').escape();


    req.sanitize('donation').trim();
    req.sanitize('donation').escape();

    let donation = parseInt(req.body.donation);

    try {
        const donated = await donate(req.user, req.body.questionId, donation)
        if (donated.success) {
            return res.json({
                data: donated.data,
            });
        }
        return res.status(500).send({ error: donated.error });
    } catch (error) {
        return res.status(500).send({ error: error.message });
    }
};


