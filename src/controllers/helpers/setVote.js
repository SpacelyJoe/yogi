/** This is going to be the function that returns true if were a subscriber OR the creator and false if were not.
 * @param {sequelize instance} userquestion - the relationship object between the user and the question
 * @param {sequelize instance} question - the actual question object.
 * @param {string} oldVote - the vote in which we had stored in the database
 * @param {string} newVote - the vote we got supplied for the update from the client
 * @return {boolean} returns true if the user is an influencer, false if they are not.
 * trying to change
 */

module.exports = async function(oldVote, newVote, userquestion, question){
    let upvotes;
    let voted;
    switch (oldVote) {
        case 'none':
            //update the userquestion to the newest vote since there is no vote yet.
            switch (newVote) {
                case 'none':
                    return ({
                        success: false,
                        error: 'You already had no vote on this question',
                    });
                    break;
                case 'up':
                    // update the database by adding 1 from upvotes and changing the user's vote.
                    const [newQuestion, newUserQuestion] = await Promise.all([question.update({ upvotes: question.dataValues.upvotes + 1}),
                                                                                userquestion.update({ voted: 'up'})
                                                                                ]);

                    console.log('the new userquestion and question were', newUserQuestion.dataValues, newQuestion.dataValues);

                    upvotes = newQuestion.dataValues.upvotes;
                    voted = newUserQuestion.dataValues.voted;
                    break;
            }
            break;
        case 'up':
            switch (newVote) {
                case 'none':
                    // update the database by subtracting 1 from upvotes and changing the user's vote.
                    const [newQuestion, newUserQuestion] = await Promise.all([question.update({ upvotes: question.dataValues.upvotes - 1}),
                        userquestion.update({ voted: 'none'})]);
                    

                    upvotes = newQuestion.dataValues.upvotes;
                    voted = newUserQuestion.dataValues.voted;
                    break;
                case 'up':
                    return ({
                        success: false,
                        error: 'You already upvoted this question.',
                    });
                    break;
            }
            break;
    }
    return ({
        success: true,
        upvotes,
        voted,
    });
}