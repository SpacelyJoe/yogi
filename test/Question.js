// During the test the env variable is set to test
process.env.NODE_ENV = 'test';

const bcrypt = require('bcryptjs');

// Require the dev-dependencies
const chai = require('chai');
const expect = require('chai').expect;
const chaiHttp = require('chai-http');
const request = require('supertest');

const User = require('../src/models/User');
const server = require('../src/server');
const db = require('../src/db');

chai.use(chaiHttp);
// Our parent block

describe('Question', () => {
    let token;
    // first make an account
    // make a second account
    // login to the first account we made
    beforeEach((done) => { // Before each test we empty the database
        db.connection.sync({force: true}) // drops all the tables and re-creates them
            .then(() => {
                const password = bcrypt.hashSync('test2', 6);
                return User.create({
                    username: 'influencer_test',
                    password,
                    email: 'testy@test.com',
                }).then(() => {
                    // make a second account
                    User.create({
                        username: 'subscriber_test',
                        password,
                        email: 'testy@tes2t.com',
                    }).then(() => {
                        request(server)
                            .post('/api/login')
                            .send({
                                username: 'subscriber_test',
                                password: 'test2',
                            })
                            .end((err, res) => {
                                token = res.body.token;
                                expect(res).to.have.status(200);
                                expect(res).to.be.a('object');
                                expect(res.body).to.have.property('token');
                                done();
                            })
                    })
                });
            });
    });

    describe('/POST/question/', () => {
        it('it should create a new question', (done) => {
            // subscribe to the channel
            const question = {
                content: 'what is your favorite color?',
                askedUserId: 1,
            };
            request(server)
                .post('/api/question/create')
                .set('Authorization', token)
                .send(question)
                .end((err, res) => {
                    console.log(res.body);
                    expect(res).to.have.status(200);
                    expect(res).to.be.a('object');
                    console.log('the body -', res.body);
                    expect(res.body).to.have.property('content');
                    done();
                });
        });
    });

    describe('/POST/question/', () => {
        it('it should not create a new question on the post if the asked user doesnt exist', (done) => {
            // subscribe to the channel
            const question = {
                content: 'what is your favorite color?',
                askedUserId: 1656,
            };
            request(server)
                .post('/api/question/create')
                .set('Authorization', token)
                .send(question)
                .end((err, res) => {
                    console.log('the response we gettin is', res.body.error);
                    console.log('the error we gettin is', err);
                    expect(res).to.have.status(500);
                    expect(res).to.be.a('object');
                    console.log('the body -', res.body);
                    expect(res.body).to.have.property('error');
                    done();
                });
        });
    });

    describe('/GET/questions/userWasAsked/:status/:queryMethod/:userId/:skip', () => {
        it('it should get the 2 top cooking questions based on the user that was asked', (done) => {
            // subscribe to the channel
            let question = {
                content: 'first question?',
                askedUserId: 1,
            };
            request(server)
                .post('/api/question/create')
                .set('Authorization', token)
                .send(question)
                .end((err, res) => {
                    console.log('the error', err);
                    console.log(res.body);
                    expect(res).to.have.status(200);
                    expect(res).to.be.a('object');
                    expect(res.body).to.have.property('content');
                    question = {
                        content: 'second question?',
                        askedUserId: 1,
                    };
                    request(server)
                        .post('/api/question/create')
                        .set('Authorization', token)
                        .send(question)
                        .end((err, res2) => {
                            console.log('the error', err);
                            console.log(res2.body);
                            expect(res2).to.have.status(200);
                            expect(res2).to.be.a('object');
                            expect(res2.body).to.have.property('content');
                            request(server)
                                .get('/api/questions/userWasAsked/cooking/top/' + question.askedUserId + '/0')
                                .set('Authorization', token)
                                .end((err3, res3) => {
                                    console.log(res2.body);
                                    expect(res3).to.have.status(200);
                                    console.log('the body -', res2.body);
                                    expect(res3.body.length).to.equal(2);
                                    expect(res3.body[0]).to.have.property('content');
                                    expect(res3.body[0].content).to.equal('first question?');
                                    expect(res3.body[1]).to.have.property('content');
                                    expect(res3.body[1].content).to.equal('second question?');
                                    done();
                                })
                        })
                });
        });
    });
    

    describe('/GET/questions/userWasAsked/:status/:queryMethod/:userId/:skip', () => {
        it('it should get the 2 new cooking questions based on the user that was asked', (done) => {
            // subscribe to the channel
            let question = {
                content: 'first question?',
                askedUserId: 1,
            };
            request(server)
                .post('/api/question/create')
                .set('Authorization', token)
                .send(question)
                .end((err, res) => {
                    console.log('the error', err);
                    console.log(res.body);
                    expect(res).to.have.status(200);
                    expect(res).to.be.a('object');
                    expect(res.body).to.have.property('content');
                    question = {
                        content: 'second question?',
                        askedUserId: 1,
                    };
                    request(server)
                        .post('/api/question/create')
                        .set('Authorization', token)
                        .send(question)
                        .end((err, res2) => {
                            console.log('the error', err);
                            console.log(res2.body);
                            expect(res2).to.have.status(200);
                            expect(res2).to.be.a('object');
                            expect(res2.body).to.have.property('content');
                            request(server)
                                .get('/api/questions/userWasAsked/cooking/new/' + question.askedUserId + '/0')
                                .set('Authorization', token)
                                .end((err3, res3) => {
                                    console.log(res2.body);
                                    expect(res3).to.have.status(200);
                                    console.log('the body -', res2.body);
                                    expect(res3.body.length).to.equal(2);
                                    expect(res3.body[0]).to.have.property('content');
                                    expect(res3.body[0].content).to.equal('first question?');
                                    expect(res3.body[1]).to.have.property('content');
                                    expect(res3.body[1].content).to.equal('second question?');
                                    done();
                                })
                        })
                });
        });
    });

    describe('/POST/question/', () => {
        it('it should return no questions when we try to query an answered question from the user who was asked when there is none in the basket', (done) => {
            // subscribe to the channel
            const question = {
                content: 'what is your favorite color?',
                askedUserId: 1,
            };
            request(server)
                .post('/api/question/create')
                .set('Authorization', token)
                .send(question)
                .end((err, res) => {
                    console.log('the error', err);
                    console.log(res.body);
                    expect(res).to.have.status(200);
                    expect(res).to.be.a('object');
                    expect(res.body).to.have.property('content');
                    request(server)
                        .get('/api/questions/userWasAsked/inBasket/new/' + question.askedUserId + '/0')
                        .set('Authorization', token)
                        .end((err2, res2) => {
                            console.log(res2.body);
                            console.log('the body -', res2.body);
                            expect(res2).to.have.status(200);
                            expect(res2.body.length).to.equal(0);
                            done();
                        })
                });
        });
    });

    describe('/POST/question/', () => {
        it('it should return no questions when we try to query an answered question from the user who asked when there is none answered', (done) => {
            // subscribe to the channel
            const question = {
                content: 'what is your favorite color?',
                askedUserId: 1,
            };
            request(server)
                .post('/api/question/create')
                .set('Authorization', token)
                .send(question)
                .end((err, res) => {
                    console.log('the error', err);
                    console.log(res.body);
                    expect(res).to.have.status(200);
                    expect(res).to.be.a('object');
                    expect(res.body).to.have.property('content');
                    request(server)
                        .get('/api/questions/userWasAsked/cooking/new/' + 2 + '/0')
                        .set('Authorization', token)
                        .end((err2, res2) => {
                            console.log('the body -', res2.body);
                            console.log(res2.body);
                            expect(res2).to.have.status(200);
                            expect(res2.body.length).to.equal(0);
                            done();
                        })
                });
        });
    });





    // VOTING ROUTE TESTING -----------------------------------------------------------------------------------------------------


    describe('/PUT/question/vote', () => {
        it('it should return that the question was upvoted and increase the upvotes by 1 if the old vote was none', (done) => {
            // subscribe to the channel
            const question = {
                content: 'what is your favorite color?',
                askedUserId: 1,
            };
            request(server)
                .post('/api/question/create')
                .set('Authorization', token)
                .send(question)
                .end((err, res) => {
                    console.log('the error', err);
                    console.log(res.body);
                    expect(res).to.have.status(200);
                    expect(res).to.be.a('object');
                    expect(res.body).to.have.property('content');
                    let vote = {
                        questionId: 1,
                        vote: 'up',
                    }
                    request(server)
                        .put('/api/question/vote')
                        .set('Authorization', token)
                        .send(vote)
                        .end((err2, res2) => {
                            console.log('the body -', res2.body);
                            console.log(res2.body);
                            expect(res2).to.have.status(200);
                            expect(res2.body).to.have.property('voted');
                            expect(res2.body).to.have.property('upvotes');
                            expect(res2.body.upvotes).to.equal(1);
                            expect(res2.body.voted).to.equal('up');
                            done();
                        })
                });
        });
    });

    describe('/PUT/question/vote', () => {
        it('it should return that the question was set to none and decrease the upvotes by 1 if the old vote was up', (done) => {
            // subscribe to the channel
            const question = {
                content: 'what is your favorite color?',
                askedUserId: 1,
            };
            request(server)
                .post('/api/question/create')
                .set('Authorization', token)
                .send(question)
                .end((err, res) => {
                    console.log('the error', err);
                    console.log(res.body);
                    expect(res).to.have.status(200);
                    expect(res).to.be.a('object');
                    expect(res.body).to.have.property('content');
                    let vote = {
                        questionId: 1,
                        vote: 'up',
                    }
                    request(server)
                        .put('/api/question/vote')
                        .set('Authorization', token)
                        .send(vote)
                        .end((err2, res2) => {
                            console.log('the body -', res2.body);
                            console.log(res2.body);
                            expect(res2).to.have.status(200);
                            expect(res2.body).to.have.property('voted');
                            expect(res2.body).to.have.property('upvotes');
                            expect(res2.body.upvotes).to.equal(1);
                            expect(res2.body.voted).to.equal('up');
                             vote = {
                                questionId: 1,
                                vote: 'none',
                            }
                            request(server)
                                .put('/api/question/vote')
                                .set('Authorization', token)
                                .send(vote)
                                .end((err2, res2) => {
                                    console.log('the body -', res2.body);
                                    console.log(res2.body);
                                    expect(res2).to.have.status(200);
                                    expect(res2.body).to.have.property('voted');
                                    expect(res2.body).to.have.property('upvotes');
                                    expect(res2.body.upvotes).to.equal(0);
                                    expect(res2.body.voted).to.equal('none');
                                    done();
                                })
                        })
                });
        });
    });

    describe('/PUT/question/vote', () => {
        it('it should return an error that a question can not be upvoted if you already upvoted it.', (done) => {
            // subscribe to the channel
            const question = {
                content: 'what is your favorite color?',
                askedUserId: 1,
            };
            request(server)
                .post('/api/question/create')
                .set('Authorization', token)
                .send(question)
                .end((err, res) => {
                    console.log('the error', err);
                    console.log(res.body);
                    expect(res).to.have.status(200);
                    expect(res).to.be.a('object');
                    expect(res.body).to.have.property('content');
                    let vote = {
                        questionId: 1,
                        vote: 'up',
                    }
                    request(server)
                        .put('/api/question/vote')
                        .set('Authorization', token)
                        .send(vote)
                        .end((err2, res2) => {
                            console.log('the body -', res2.body);
                            console.log(res2.body);
                            expect(res2).to.have.status(200);
                            expect(res2.body).to.have.property('voted');
                            expect(res2.body).to.have.property('upvotes');
                            expect(res2.body.upvotes).to.equal(1);
                            expect(res2.body.voted).to.equal('up');
                            request(server)
                                .put('/api/question/vote')
                                .set('Authorization', token)
                                .send(vote)
                                .end((err3, res3) => {
                                    console.log('the body -', res3.body);
                                    console.log(res3.body);
                                    expect(res3).to.have.status(500);
                                    expect(res3.body).to.have.property('error');
                                    expect(res3.body.error).to.equal('You already upvoted this question.');
                                    done();
                                })
                        })
                });
        });
    });

    describe('/PUT/question/vote', () => {
        it('it should return an error that a vote can not be set to none if it already is none.', (done) => {
            // subscribe to the channel
            const question = {
                content: 'what is your favorite color?',
                askedUserId: 1,
            };
            request(server)
                .post('/api/question/create')
                .set('Authorization', token)
                .send(question)
                .end((err, res) => {
                    console.log('the error', err);
                    console.log(res.body);
                    expect(res).to.have.status(200);
                    expect(res).to.be.a('object');
                    expect(res.body).to.have.property('content');
                    let vote = {
                        questionId: 1,
                        vote: 'none',
                    }
                    request(server)
                        .put('/api/question/vote')
                        .set('Authorization', token)
                        .send(vote)
                        .end((err2, res2) => {
                            console.log('the body -', res2.body);
                            console.log(res2.body);
                            expect(res2).to.have.status(500);
                            expect(res2.body).to.have.property('error');
                            expect(res2.body.error).to.equal('You already had no vote on this question');
                            done();
                        });

                })
        });
    });



    describe('/PUT/question/vote', () => {
        it('it should upvote a question and add 1 to the upvotes even if we had no relationship to the question', (done) => {
            // subscribe to the channel
            const question = {
                content: 'what is your favorite color?',
                askedUserId: 1,
            };
            request(server)
                .post('/api/question/create')
                .set('Authorization', token)
                .send(question)
                .end((err, res) => {
                    console.log('the error', err);
                    console.log(res.body);
                    expect(res).to.have.status(200);
                    expect(res).to.be.a('object');
                    expect(res.body).to.have.property('content');

                    const password = bcrypt.hashSync('test2', 6);
                    User.create({
                        username: 'voting_test',
                        password,
                        email: 'terrsty@tes2t.com',
                    }).then(() => {
                        request(server)
                            .post('/api/login')
                            .send({
                                username: 'voting_test',
                                password: 'test2',
                            })
                            .end((err2, res2) => {
                                token = res2.body.token;
                                expect(res2).to.have.status(200);
                                expect(res2).to.be.a('object');
                                expect(res2.body).to.have.property('token');

                                let vote = {
                                    questionId: 1,
                                    vote: 'up',
                                }
                                request(server)
                                    .put('/api/question/vote')
                                    .set('Authorization', token)
                                    .send(vote)
                                    .end((err3, res3) => {
                                        console.log('the body -', res3.body);
                                        console.log(res3.body);
                                        expect(res3).to.have.status(200);
                                        expect(res3.body).to.have.property('voted');
                                        expect(res3.body).to.have.property('upvotes');
                                        expect(res3.body.upvotes).to.equal(1);
                                        expect(res3.body.voted).to.equal('up');
                                        done();
                                    });

                            })
                    });
                });

        })
    });


});


function askQuestion(times, token, cb){
    let j = 0;
    const question = {
        content: 'what is your favorite color?',
        askedUserId: 1,
    };
    for(var i = 0; i < times; i++) {
        request(server)
            .post('/api/question/create')
            .set('Authorization', token)
            .send(question)
            .end((err, res) => {
                j++;
                if(j >= times) {
                    cb();
                }
             });
    }
}