const Sequelize = require('sequelize');

const db = require('../db');

const User = require('./User');
const Question = require('./Question');

const UserQuestion = db.connection.define('UserQuestions', {
    hasAccess: {
        type: Sequelize.BOOLEAN,
        unique: false,
        allowNull: false,
        defaultValue: false,
    },
    donatedCents: {
        type: Sequelize.INTEGER,
        unique: false,
        allowNull: false,
        defaultValue: 0,
    },
    voted: {
        type: Sequelize.ENUM('none', 'up'),
        unique: false,
        allowNull: false,
        defaultValue: 'none',
    },
});

Question.belongsToMany(User, { through: UserQuestion });




module.exports = UserQuestion;
