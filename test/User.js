// During the test the env variable is set to test
process.env.NODE_ENV = 'test';

const bcrypt = require('bcryptjs');

// Require the dev-dependencies
const chai = require('chai');
const expect = require('chai').expect;
const chaiHttp = require('chai-http');
const request = require('supertest');

const User = require('../src/models/User');

const server = require('../src/server');
const db = require('../src/db');

chai.use(chaiHttp);
// Our parent block

describe('User', () => {
    beforeEach((done) => { // Before each test we empty the database
        db.connection.sync({ force: true }) // drops all the tables and re-creates them
            .then(() => {
                done(null);
            })
            .catch(error => {
                done(error);
            });
    });

    describe('/POST register', () => {
        it('it should POST a registered user', (done) => {
            const user = {
                username: 'joeytest',
                password: 'joetest',
                password2: 'joetest',
                email: 'joetest@test.com',
            };
            request(server)
                .post('/api/register')
                .send(user)
                .end((err, res) => {
                    console.log('the res:', res.body);
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.a('object');

                    expect(res.body).to.have.property('object');
                    done();
                });
        });
    });

    describe('/POST register', () => {
        it('it should not POST a registered user if the username is already in use', (done) => {
            const user = {
                username: 'joeytest',
                password: 'joetest',
                password2: 'joetest',
                email: 'joetest@test.com',
            };

            // first create a test user with the name we want to use again
            User.create({
                username: 'joeytest',
                password: 'test1',
                email: 'testy@test.com',
            }).then(() => {
                request(server)
                    .post('/api/register')
                    .send(user)
                    .end((err, res) => {
                        expect(res).to.have.status(500);
                        done();
                    });
            });
        });
    });

    describe('/POST login', () => {
        it('it authenticates and login a user', (done) => {
            const password = bcrypt.hashSync('test2', 6);
            User.create({
                username: 'test3',
                password,
                email: 'testy@test.com',
            }).then(() => {
                request(server)
                    .post('/api/login')
                    .send({ username: 'test3',
                        password: 'test2' })
                    .end((err, res) => {
                        expect(res).to.have.status(200);
                        expect(res.body).to.be.a('object');
                        console.log('the res.body is:', res.body);
                        expect(res.body).to.have.property('token');
                        done();
                    });
            });
        });
    });


    describe('/POST login', () => {
        it('it does NOT authenticate a user if the password is wrong', (done) => {
            const password = bcrypt.hashSync('test2', 6);
            User.create({
                username: 'test3',
                password,
                email: 'testy@test.com',
            }).then(() => {
                request(server)
                    .post('/api/login')
                    .send({ username: 'test3',
                        password: 'test443452' })
                    .end((err, res) => {
                        expect(res).to.have.status(500);
                        done();
                    });
            });
        });
    });

    describe('/POST login', () => {
        it('it does NOT authenticate a user if the user doesnt exist', (done) => {
            const password = bcrypt.hashSync('test2', 6);
            User.create({
                username: 'test3',
                password,
                email: 'testy@test.com',
            }).then(() => {
                request(server)
                    .post('/api/login')
                    .send({ username: 'test345234523453',
                        password: 'test443452' })
                    .end((err, res) => {
                        expect(res).to.have.status(500);
                        done();
                    });
            });
        });
    });
});
