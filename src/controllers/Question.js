const Question = require('../models/Question');
const UserQuestion = require('../models/UserQuestion');
const User = require('../models/User');
const validate = require('./helpers/validate');

const setVote = require('./helpers/setVote');

require('../models/UserQuestion');

/** this will make sure the user is a subscriber of the group and
 * and then post the answer
 * @param {integer} askingUser - user object who is asking the question
 * @param {integer} askedUserId - ID of the user were asking the question
 * @param {string} content - the content of the question
 * @return {JSON} returns success true and the post if it worked, and success false and an error message
 * if it doesnt work.
 */
async function create(askingUser, askedUserId, content) {
    try {
        const askedUser = await User.findById(askedUserId);

        if (!validate.objExists(askingUser)) {
            return ({
                success: false,
                error: 'The asking user does not exist.',
            });
        }

        if (!validate.objExists(askedUser)) {
            return ({
                success: false,
                error: 'The asked user does not exist.',
            });
        }

        let question = await Question.create({
            content,
            AskerId: askingUser.dataValues.id,
            AnswererId: askedUser.dataValues.id,
            answered: false,
        });

        await Promise.all([question.addUser(askedUser, { hasAccess: true }), question.addUser(askingUser, { hasAccess: true })]);
 
        return ({
            success: true,
            question: question.dataValues,
        });
    } catch (error) {
        console.log(error);
        return ({
            success: false,
            error: error.message,
        });
    }
};



/** this will get all the pending questions of a user.
 * @param {userId} int - ID of the user that was asked the questions
 * @param {status} ENUM/STRING - status of the type of question we want to query, either 'onTable', 'inBasket', 'cooking'
 * @param {queryMethod} string - The attribute were querying the questions by, its either by upvotes(top) or by significantlyUpdatedAt(new)
 * @param {skip} int - the number of questions we want to skip when querying.
 * @return {JSON} returns success true and the post if it worked, and success false and an error message
 * if it doesnt work.
 */
async function getQuestionsUserWasAsked(userId, status, queryMethod, skip) {

    //these are the variables we use when we want to decide if were querying by new or top.
    let order;
    let attribute;
    switch(queryMethod){
        case('new'):
            attribute = 'significantlyUpdatedAt';
            order = 'ASC';
            break;
        case('top'):
            attribute = 'upvotes';
            order = 'DESC'
            break;
    }

    try {
        const questions = await Question.findAll({
            offset: skip,
            limit: 5,
            order: [[attribute, order]],
            include: [{
                model: User,
                as: 'Asker',
                attributes: ['username', 'profilePic'],
            }],
            where: {
                AnswererId: userId,
                status,
            }
        });
        console.log('the questions was', questions);
        return({
            success: true,
            questions,
        });
    } catch (error) {
        return ({
            success: false,
            error: error.message,
        })
    }
}



/** this will get all the pending questions of a user.
 * @param {integer} userID - ID of the user that asked the questions
 * @return {JSON} returns success true and the post if it worked, and success false and an error message
 * if it doesnt work.
 */
async function getQuestionsUserAsked(userId, answered) {
    try {
        const questions = await Question.findAll({
            include: [{
                model: User,
                as: 'Answerer',
                attributes: ['username', 'profilePic'],
            }],
            where: {
                AskerId: userId,
                answered,
            }
        });
        return({
            success: true,
            questions,
        });
    } catch (error) {
        return ({
            success: false,
            error: error.message,
        })
    }
}

/** this will submit a vote on the question
 * @param {integer} userId - ID of the user thats attempting to vote on the question
 * @param {integer} questionId - ID of the question were trying to vote on
 * @param {string} vote - status of the vote, either up or down.
 * @return {JSON} returns success true and the post if it worked, and success false and an error message
 * if it doesnt work.
 */

async function vote(user, questionId, newVote){
    try {

        // first get the question to make sure its a valid question.

        const question = await Question.findById(questionId);

        // if it returns null, we need to tell them it doesnt exist.

        if(question === null) {
            return ({
                success: false,
                error: 'This question does not exist.'
            })
        }

        // if it does exist then we want to get the relationship object the user has with the object so we
        // can see if theyve voted already.

        const userquestion = await UserQuestion.find({
            where: {
                UserId: user.id,
                QuestionId: questionId,
        }})

        // if it comes back as null, that means theres no relationship yet.
        if(userquestion === null){
            // only make the relationship if the new vote is up, cause if its not that means
            // theres no point in them trying to vote.
            if(newVote === 'up') {
                // create the new userquestion relationship and also update the question's votes.
                let [newUserquestion, newQuestion] = await Promise.all([question.addUser(user, { voted: newVote }),
                                                        question.update({ upvotes: question.dataValues.upvotes + 1})]);

                return ({
                    success: true,
                    upvotes: newQuestion.dataValues.upvotes,
                    voted: newUserquestion[0][0].dataValues.voted,
                });
            }else {
                return({
                    success: false,
                    error: 'Please only upvote questions you havent voted on. Like cmon man what you tryna do',
                })
            }
        }else {
            let voted = await setVote(userquestion.dataValues.voted, newVote, userquestion, question)
            console.log('our voted obj is', voted);
            return(voted);
        }
    } catch (error) {
        return ({
            success: false,
            error: error.message,
        })
    }
}




// -----------------------------------------------------------------------------------------------------
// these are the functions that our routes will use and work as a middle man
// passing in the variables needed for the actual controller functions
// ------------------------------------------------------------------------------------------------------

module.exports.createAPI = async function(req, res) {
    if (!req.body.askedUserId || !req.body.content) {
        return (res.json({ success: false, error: 'Please provide the valid parameters' }));
    }

    req.sanitize('askedUserId').trim();
    req.sanitize('askedUserId').escape();


    req.sanitize('content').trim();
    req.sanitize('content').escape();


    try {
        const created = await create(req.user, req.body.askedUserId, req.body.content);
        if(created.success) {
            return res.json(created.question);
        }
        return res.status(500).send({ error: created.error });
    } catch (error) {
        return res.status(500).send({ error: error.message });
    }
};

module.exports.getQuestionsUserAskedAPI = async function(req, res) {

    let answered;
    if(req.params.answered === 'false' || !req.params.answered){
        answered = false;
    } else if (req.params.answered === 'true' || req.params.answered){
        answered = true;
    } else {
        return res.status(500).send({ error: 'Please provide the valid parameters' });
    }


    if (!req.params.userId){
        return res.status(500).send({ error: 'Please provide the valid parameters' });
    }

    req.sanitize(req.param('userId')).trim();
    req.sanitize(req.param('userId')).escape();

    req.sanitize(req.param('answered')).trim();
    req.sanitize(req.param('answered')).escape();

    try {
        const questions = await getQuestionsUserAsked(req.params.userId, answered);
        console.log('the second questions is', questions)
        if(questions.success) {
            return res.json(questions.questions);
        }
        return res.status(500).send({ error: questions.error });
    } catch (error) {
        return res.status(500).send({ error: error.message });
    }
};


module.exports.getQuestionsUserWasAskedAPI = async function(req, res) {
    let answered;
    if(req.params.status !== 'onTable' && req.params.status !== 'inBasket' && req.params.status !== 'cooking'){
        return res.status(500).send({ error: 'Please provide a valid status for the questions you want to query.' });
    }
    
    if(req.params.queryMethod !== 'new' && req.params.queryMethod !== 'top'){
        return res.status(500).send({ error: 'Please provide the valid parameters for the query type.' });
    }

    req.sanitize(req.param('userId')).trim();
    req.sanitize(req.param('userId')).escape();

    req.sanitize(req.param('status')).trim();
    req.sanitize(req.param('status')).escape();

    req.sanitize(req.param('skip')).trim();
    req.sanitize(req.param('skip')).escape();

    req.sanitize(req.param('queryMethod')).trim();
    req.sanitize(req.param('queryMethod')).escape();

    let skip = parseInt(req.params.skip);

    try {
        // use the function supplied with our req params. 
        const questions = await getQuestionsUserWasAsked(req.params.userId, req.params.status, req.params.queryMethod, skip);
        if(questions.success) {
            return res.json(questions.questions);
        }
        return res.status(500).send({ error: questions.error });
    } catch (error) {
        return res.status(500).send({ error: error.message });
    }
};

module.exports.getTopQuestionsUserWasAskedAPI = async function(req, res) {

    let answered;
    if(req.params.answered === 'false' || !req.params.answered){
        answered = false;
    } else if (req.params.answered === 'true' || req.params.answered){
        answered = true;
    } else {
        return res.status(500).send({ error: 'Please provide the valid parameters' });
    }


    if (!req.params.userId && !req.params.skip) {
        return (res.json({ success: false, error: 'Please provide a user Id' }));
    }

    req.sanitize(req.param('userId')).trim();
    req.sanitize(req.param('userId')).escape();

    req.sanitize(req.param('skip')).trim();
    req.sanitize(req.param('skip')).escape();

    let skip = parseInt(req.params.skip);

    try {
        const questions = await getTopQuestionsUserWasAsked(req.params.userId, answered, skip);
        console.log('questions should be false here', questions)
        if(questions.success) {
            return res.json(questions.questions);
        }
        return res.status(500).send({ error: questions.error });
    } catch (error) {
        return res.status(500).send({ error: error.message });
    }
};


module.exports.getTopUnAnsweredQuestionsUserWasAskedAPI = async function(req, res) {

    if (!req.params.userId && !req.params.skip) {
        return (res.json({ success: false, error: 'Please provide a user Id' }));
    }

    req.sanitize(req.param('userId')).trim();
    req.sanitize(req.param('userId')).escape();

    req.sanitize(req.param('skip')).trim();
    req.sanitize(req.param('skip')).escape();

    let skip = parseInt(req.params.skip);

    try {
        const questions = await getTopQuestionsUserWasAsked(req.params.userId, false, skip);
        console.log('questions should be false here', questions)
        if(questions.success) {
            return res.json(questions.questions);
        }
        return res.status(500).send({ error: questions.error });
    } catch (error) {
        return res.status(500).send({ error: error.message });
    }
};



module.exports.voteAPI = async function(req, res) {

    if (!req.body.questionId || !req.body.vote) {
        return (res.json({success: false, error: 'Please provide a question Id and a vote.'}));
    }

    if(req.body.vote !== 'up' && req.body.vote !== 'none') {
        return (res.json({ success: false, error: 'please provide a valid vote' }));
    }

    req.sanitize('vote').trim();
    req.sanitize('vote').escape();

    req.sanitize('questionId').trim();
    req.sanitize('questionId').escape();
    try {
        const voted = await vote(req.user, req.body.questionId, req.body.vote)
        if(voted.success) {
            return res.json({
                voted: voted.voted,
                upvotes: voted.upvotes,
            });
        }
        return res.status(500).send({ error: voted.error });
    } catch (error) {
        return res.status(500).send({ error: error.message });
    }
};
