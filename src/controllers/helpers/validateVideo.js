const aws = require('aws-sdk');
const config = require('config');
const ffmpeg = require('fluent-ffmpeg');


/** this will upload an answer to the s3 bucket if theres no answer yet.
 * This function will take the metadata from the s3 video that the client is sending to check for their answer
 * and will return as a success if it was less than the durated time
 */
module.exports = async function(maxDuration, videoKey) {

    aws.config.update({ accessKeyId: config.AWS_ACCESS_KEY, secretAccessKey: config.AWS_SECRET_KEY });

    const s3 = new aws.S3();

    const params = { Bucket: 'yogi-answers', Key: videoKey };


    var command = new ffmpeg();
    ffmpeg.setFfmpegPath('C:\\Users\\J\\Downloads\\ffmpeg-3.1.4-win32-static\\ffmpeg-3.1.4-win32-static\\bin\\ffmpeg.exe');
    ffmpeg.setFfprobePath('C:\\Users\\J\\Downloads\\ffmpeg-3.1.4-win32-static\\ffmpeg-3.1.4-win32-static\\bin\\ffprobe.exe');


    /*
     let writer = fs.createWriteStream('../temp_videos/' + req.params.videoKey);
     let reader = s3.getObject(params).createReadStream();
     reader.pipe(writer, { end: false });
     reader.on('end', function () {
     */

    console.log('cuz bro it gets right fucking before here');

    ffmpeg.ffprobe('https://s3.amazonaws.com/yogi-answers/shortvideo.mp4', function(err, metadata){
        console.log(metadata);
        if(err){
            return({success: false,
                    error: err,
            })
        }
        let duration = metadata.streams[0].duration;

        if(duration <= maxDuration){
            return ({ 'success': true })
        } else {
            return ({
                'success': false,
                'error': 'Video was over the time limit of 60 seconds.'
            })
        }
    })
}