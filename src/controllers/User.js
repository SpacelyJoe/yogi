const bcrypt = require('bcryptjs');
const config = require('config');
const jwt = require('jsonwebtoken');
// bring in the passport strategy we definied


const User = require('../models/User');

function generateToken(user) {
    return jwt.sign(user, config.secretAuthKey, {
        expiresIn: 10080, // in seconds
    });
}

function setUserInfo(user) {
    const getUserInfo = {
        id: user.id,
        username: user.username,
        email: user.email,
        profile_pic: user.profile_pic,
        centsOnAccount: user.centsOnAccount,
    };
    return getUserInfo;
}


module.exports.authenticate = function (req, res) {
    console.log('the username', req.body.username);
    User.findOne({
        where: {
            username: req.body.username,
        },
    }).then(user => {
        if (!user) {
            res.status(500).send('User not found.');
        } else {
            bcrypt.compare(req.body.password, user.dataValues.password, (err, isMatch) => {
                if (err) res.status(500).send('Error');
                if (isMatch) {
                    const userInfo = setUserInfo(user.dataValues);
                    res.json({
                        token: 'JWT ' + generateToken(userInfo),
                        user: userInfo,
                    });
                } else {
                    return res.status(500).send('Password invalid');
                }
            });
        }
    });
};

module.exports.register = function (req, res) {
    req.checkBody('username', 'Name is required').notEmpty();
    req.sanitize('username').trim();
    req.sanitize('username').escape();
    req.checkBody('username', 'Your username can only ' +
        'contain letters a-Z and underscores').isValidUsernameCharacter();
    req.checkBody('username', 'Your username can only ' +
        'be 1-18 characters long').isValidUsernameLength();

    req.checkBody('email', 'Email is required').notEmpty();
    req.checkBody('email', 'Email is not valid').isEmail();
    req.sanitize('email').escape();

    req.checkBody('username', 'Username is required').notEmpty();
    req.checkBody('password', 'Paswsword is required').notEmpty();
    req.checkBody('password2', 'Passwords do not match').equals(req.body.password);

    const errors = req.validationErrors();

    if (errors) {
        // if there are errors we want to rerender the form and pass along the
        // erors as a json object
        res.status(500).send(errors);
    }

    const username = req.body.username;
    const email = req.body.email;

    bcrypt.hash(req.body.password, 12, async function(error, hash) {
        if (error) {
            res.status(500).send('Internal server error');
        }
        try {
            const user = await User.create({
                username,
                password: hash,
                email,
            });

            return res.json({
                user: {
                    username: user.username,
                    email: user.email,
                },
            });
        } catch (error2) {
            return res.status(500).send({ error: error2.message });
        }
    });
    return undefined;
};

module.exports.getUserByName = async function(username) {
    const user = await User.find({
        where: {
            username,
        },
    });
    if (user === undefined || user === null) {
        return new Error('The user doesn\'t exist');
    }
    return user;
};

module.exports.findUser = async function(req, res) {
    req.sanitize(req.param('id'));

    const user = await User.find({
        where: {
            id: req.params.id,
        },
    });

    if (user instanceof Error || user === null) {
        return res.status(500).send('User not found');
    }

    return res.json({
        status: 'success',
        object: {
            username: user.username,
            profilePic: user.profilePic,
            email: user.email,
            id: user.id,
        },
    });
};
