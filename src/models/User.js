const Sequelize = require('sequelize');

const db = require('../db');

const User = db.connection.define('Users', {
    username: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false,
    },
    password: {
        type: Sequelize.STRING,
        unique: false,
        allowNull: false,
    },
    email: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false,
    },
    profilePic: {
        type: Sequelize.STRING,
        unique: false,
        allowNull: false,
        defaultValue: 'https://s3.amazonaws.com/snooply/lxwHAuTtkfJKJjh',
    },
    type: {
        type: Sequelize.ENUM('sports', 'fitness', 'music', 'politics', 'other'),
        unique: false,
        allowNull: false,
        defaultValue: 'other',
    },
    centsOnAccount: {
        type: Sequelize.INTEGER,
        unique: false,
        allowNull: false,
        defaultValue: 0,
    },
});

module.exports = User;
