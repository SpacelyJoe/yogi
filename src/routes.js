const express = require('express');
const config = require('config');
const jwt = require('jsonwebtoken');
const passport = require('passport');
// bring in the passport strategy we definied
require('./passport_auth')(passport);


const router = express.Router();
const UserController = require('./controllers/User');
const QuestionController = require('./controllers/Question');
const AnswerController = require('./controllers/Answer');

require('./passport_auth')(passport);

const requireAuth = passport.authenticate('jwt', { session: false });

router.route('/register')
    .post(UserController.register);

router.route('/login')
    .post(UserController.authenticate);

router.route('/question/create')
    .post(requireAuth, QuestionController.createAPI);

router.route('/question/vote')
    .put(requireAuth, QuestionController.voteAPI);


router.route('/questions/userWasAsked/:answered/:userId')
    .get(requireAuth, QuestionController.getQuestionsUserWasAskedAPI);


// the status is either onTable, inBasket, or cooking. The queryMethod is either new or top.  
router.route('/questions/userWasAsked/:status/:queryMethod/:userId/:skip')
    .get(requireAuth, QuestionController.getQuestionsUserWasAskedAPI);


router.route('/questions/userAsked/:answered/:userId')
    .get(requireAuth, QuestionController.getQuestionsUserAskedAPI);



router.route('/answer/upload')
    .post(requireAuth, AnswerController.uploadAPI);
router.route('/answer/donate')
    .put(requireAuth, AnswerController.donateAPI);

router.route('/answer/create')
    .post(requireAuth, AnswerController.createAPI);

module.exports = router;
