const bodyParser = require('body-parser');
const config = require('config'); // we load the db location from the JSON files
// const cookieParser = require('cookie-parser');
const express = require('express');
const expressSession = require('express-session');
const expressValidator = require('express-validator');
const http = require('http');
const morgan = require('morgan');
const passport = require('passport');

const app = express();
const server = http.createServer(app);

// our own dependencies

// const sockets = require('./SocketServer/sockets');

const port = 8080;


const sessionMiddleware = expressSession({
    secret: 'mySecretCookieSalt',
    key: 'myCookieSessionId',
    cookie: {
        httpOnly: true,
        secure: true,
        // domain: 'example.com',
        // path: '/foo/bar',
        // Cookie will expire in 1 hour from when it's generated
        expires: new Date(Date.now() + 60 * 60 * 1000),
    },
});


// initialize the socket server
// const io = sockets.start(server);

// don't show the log when it is test
if (config.util.getEnv('NODE_ENV') !== 'test') {
    // use morgan to log at command line
    app.use(morgan('combined')); // 'combined' outputs the Apache style LOGs
}

app.use(sessionMiddleware);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.text());
app.use(bodyParser.json({ type: 'application/json' }));

// Initialize passport for use
app.use(passport.initialize());

const routes = require('./routes');

app.use(expressValidator({
    errorFormatter: (param, msg, value) => {
        const namespace = param.split('.');
        const root = namespace.shift();
        let formParam = root;

        while (namespace.length) {
            formParam += '[' + namespace.shift() + ']';
        }
        return {
            param: formParam,
            msg,
            value,
        };
    },
    customValidators: {
        isValidUsernameCharacter: value => /^[a-zA-Z0-9_]+$/.test(value),
        isValidUsernameLength: value => (value.length >= 1 && value.length <= 18),
        isValidChatname: value => /^[a-zA-Z0-9 _]+$/.test(value),
        isValidChatnameLength: (value) => (value.length >= 3 && value.length <= 25),
        isValidAccountType: value => value === 'music' || value === 'sports' || value === 'politics' || value === 'fitness'
         || value === 'other',
    },
}));


app.get('/', (req, res) => res.json({ message: 'Welcome to Pak!' }));

app.use('/api', routes);

app.listen(port);
console.log('Listening on port ', port);

module.exports = app; // for testing
