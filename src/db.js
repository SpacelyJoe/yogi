const config = require('config');
const Sequelize = require('sequelize');

console.log(config.database);
console.log(config.username);
console.log(config.password);

const connection = new Sequelize(config.database, config.username, config.password);

connection.sync();

exports.connection = connection;

